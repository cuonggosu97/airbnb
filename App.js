import React from 'react'
//Pakage
import { createSharedElementStackNavigator } from "react-navigation-shared-element";
import { NavigationContainer } from "@react-navigation/native";

//Screen
import Explore from "./src/Explore";
import Listing from "./src/Listing";

const Stack = createSharedElementStackNavigator();

const App = ({ navigation }) => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={Explore}
        headerMode="none"
      >
        <Stack.Screen
          name="Explore"
          component={Explore}
        />
        <Stack.Screen
          name="Listing"
          component={Listing}
          options={navigation => ({
            headerBackTitleVisible: false,
            cardStyleInterpolator: ({ current: { progress } }) => {
              return {
                cardStyle: {
                  opacity: progress
                }
              }
            }
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App
