import React from "react";
import { StyleSheet } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { Header, Listing } from "./components";

const listings = [
  {
    id: "tiny-home",
    title: "Tiny Home",
    subtitle: "Entire Flat · 1 Bed",
    picture: require("./assets/tiny-home.jpg"),
    rating: 4.93,
    ratingCount: 861
  },
  {
    id: "cook-house",
    title: "Cook House",
    subtitle: "Entire Flat · 1 Bed",
    picture: require("./assets/cook-house.jpg"),
    rating: 4.93,
    ratingCount: 861
  }
];
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center"
  }
});

const Explore = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <Header />
      {listings.map(item => (
        <Listing key={item.id} {...{ item, navigation }} />
      ))}
    </SafeAreaView>
  );
};

export default Explore;
