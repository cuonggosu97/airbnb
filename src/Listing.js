import React from "react";
import { Dimensions, Image, StyleSheet, View } from "react-native";
import { SharedElement } from "react-navigation-shared-element";
import { SafeAreaView } from "react-native-safe-area-context";

import { Description } from "./components";
import { Listing as ListingModel } from "./components/Listing";

const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  image: {
    width,
    height: width
  },
  thumbnailOverlay: {
    ...StyleSheet.absoluteFillObject,
    padding: 16
  }
});
const Listing = props => {
  const { data } = props.route.params

  return (
    <View style={styles.container}>
      <View
        style={{
          flex: 1,
        }}
      >
        <View>
          <Image
            style={styles.image}
            resizeMode="cover"
            source={data.picture}
          />
          <SafeAreaView style={styles.thumbnailOverlay}>
          </SafeAreaView>
        </View>
        <Description />
      </View>
    </View >
  );
};

export default Listing;
