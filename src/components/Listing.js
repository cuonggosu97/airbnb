import React, { useEffect, useState } from "react";
import { Dimensions, Image, StyleSheet, Text, View } from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { SharedElement } from "react-navigation-shared-element";

const { width } = Dimensions.get("window");
const styles = StyleSheet.create({
  listing: {
    marginBottom: 16
  },
  image: {
    height: 150,
    width: width - 32,
    marginVertical: 8
  },
  title: {
    fontFamily: "CerealMedium",
    fontSize: 18
  },
  details: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 4
  },
  rating: {
    flexDirection: "row",
    alignItems: "center"
  },
  ratingLabel: {
    fontFamily: "CerealBook",
    marginLeft: 4
  },
  superhost: {
    borderColor: "black",
    borderRadius: 30,
    borderWidth: 1,
    padding: 4
  },
  superhostLabel: {
    fontSize: 10,
    fontFamily: "CerealMedium"
  }
});


export default ({ item, navigation }) => {

  return (
    <View key={item.id} style={styles.listing}>
      <TouchableWithoutFeedback
        onPress={() => {
          navigation.navigate('Listing', { data: item })
        }}
      >
        <View>
          <SharedElement id={item.id}>
            <Image
              style={styles.image}
              resizeMode="cover"
              source={item.picture}
            />
          </SharedElement>
          <View style={styles.details}>
            <View style={styles.superhost}>
              <Text style={styles.superhostLabel}>SUPERHOST</Text>
            </View>
            <View style={styles.rating}>
              <Text style={styles.ratingLabel}>
                {`${item.rating} (${item.ratingCount})`}
              </Text>
            </View>
          </View>
          <Text style={styles.title}>{item.title}</Text>
          <Text style={styles.title}>{item.subtitle}</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};
